<?php

namespace Drupal\oembed_filter\Controller;

use Drupal\Core\Config\Config;
use Drupal\Core\Controller\ControllerBase;
use Drupal\oembed_filter\OembedFilter as DrupalOembedFilter;

/**
 * Class DefaultController.
 *
 * @package Drupal\oembed_filter\Controller
 */
class DefaultController extends ControllerBase {

  const  CONFIG_KEY_SETTINGS = 'oembed_filter.settings';

  /**
   * Main.
   *
   * @return string
   *   Return Hello string.
   */
  public function configure() {
    $build = [];

    drupal_set_message('SoundCloud - ' . file_get_contents('http://soundcloud.com/oembed?url=https%3A%2F%2Fsoundcloud.com%2Fnilin%2Fkish-na-krayu&format=json'));

    $config_hashes = $this->getConfigHashes();
    $services = DrupalOembedFilter::getServiceHashes();
    $updated = array_diff_assoc($services, $config_hashes);

    if (count($updated) > 0) {
      $services = self::getConfigServices();

      foreach (DrupalOembedFilter::getOembedClasses() as $class) {
        if (isset($updated[$class])) {
          unset($services[$class]);
        }

        if (!isset($services[$class])) {
          $service = DrupalOembedFilter::getServiceInstance($class);

          $services[$class]['id'] = $service->getId();
          $services[$class]['endpoint'] = $service->getEndpoit();
          $services[$class]['format'] = $service->getFormatResponseLabel();
          $services[$class]['discovery'] = $service->getDiscoveryLabel();
          $services[$class]['patterns'] = (array) $service->getPatterns();
          $config_hashes[$class] = $service->getHash();

          foreach ((array) $service->getExamples() as $url) {
            if ($oembed_data = DrupalOembedFilter::getOembedData($service, $url)) {
              $snippet = $service->filter($oembed_data);
              $html = DrupalOembedFilter::buildHtml($snippet, $service);
              $services[$class]['examples'][] = [
                'url' => $url,
                'html' => $html,
              ];
            }
          }
        }
      }
      /** @var Config $config */
      $config = \Drupal::service('config.factory')->getEditable(self::CONFIG_KEY_SETTINGS);
      $config->set('hashes', $config_hashes)->save();
      $config->set('services', $services)->save();
    }

    $build['cp_form'] = $this->formBuilder()->getForm('Drupal\oembed_filter\Form\ControlPanelForm');

    return $build;
  }

  /**
   * Get config hashes.
   *
   * @param bool $reset
   *   If reset then clear values in config.
   *
   * @return array Config hashes.
   *   Config hashes.
   */
  public static function getConfigHashes($reset = FALSE) {
    if ($reset) {
      /** @var Config $config */
      $config = \Drupal::service('config.factory')
                       ->getEditable(self::CONFIG_KEY_SETTINGS);
      $config->clear('hashes')->save();
    }
    $hashes = \Drupal::config(self::CONFIG_KEY_SETTINGS)->get('hashes');
    return (is_array($hashes)) ? $hashes : [];
  }

  /**
   * Get config services.
   *
   * @param bool $reset
   *   If reset then clear values in config.
   *
   * @return array Config hashes.
   *   Config services.
   */
  public static function getConfigServices($reset = FALSE) {
    if ($reset) {
      /** @var Config $config */
      $config = \Drupal::service('config.factory')
                       ->getEditable(self::CONFIG_KEY_SETTINGS);
      $config->clear('services')->save();
    }
    $services = \Drupal::config(self::CONFIG_KEY_SETTINGS)->get('services');
    return (is_array($services)) ? $services : [];
  }

}
