<?php

namespace Drupal\oembed_filter\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\oembed_filter\OembedFilter as DrupalOembedFilter;

/**
 * Oembed filter.
 *
 * @Filter(
 *   id = "oembed",
 *   title = @Translation("Emerap oEmbed filter."),
 *   description = @Translation("Replacement oEmbed service link to different types of content."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   weight =  100,
 * )
 */
class OembedFilter extends FilterBase {

  /**
   * Performs the filter processing.
   *
   * @param string $text
   *   The text string to be filtered.
   * @param string $langcode
   *   The language code of the text to be filtered.
   *
   * @return \Drupal\filter\FilterProcessResult
   *   The filtered text, wrapped in a FilterProcessResult object, and possibly
   *   with associated assets, cacheability metadata and placeholders.
   *
   * @see \Drupal\filter\FilterProcessResult
   */
  public function process($text, $langcode) {
    $filter = new DrupalOembedFilter($text);
    return new FilterProcessResult($filter->apply());
  }

}
