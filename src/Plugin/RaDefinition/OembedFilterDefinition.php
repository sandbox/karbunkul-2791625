<?php

namespace Drupal\oembed_filter\Plugin\RaDefinition;

use Drupal\ra\RaDefinitionBase;
use Emerap\OembedFilter\OembedFilter;
use Emerap\Ra\RaConfig;

/**
 * Apply oembed filter to text.
 *
 * @RaDefinition(
 *   id = "oembed.filter",
 *   description = @Translation("Apply oembed filter to text")
 * )
 *
 * @link https://github.com/emerap/ra/wiki/Definition @endlink
 */
class OembedFilterDefinition extends RaDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($params) {
    $filter = new OembedFilter($params['text']);
    return $filter->apply();
  }

  /**
   * {@inheritdoc}
   */
  public function getMethodParams() {
    return [
      RaConfig::instanceParam('text'),
    ];
  }

}
