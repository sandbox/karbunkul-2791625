<?php

namespace Drupal\oembed_filter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\oembed_filter\Controller\DefaultController;

/**
 * Class ControlPanelForm.
 *
 * @package Drupal\oembed_filter\Form
 */
class ControlPanelForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oembed_filter_cp_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['services_vt'] = array(
      '#type' => 'vertical_tabs',
    );
    $services = DefaultController::getConfigServices();
    sort($services);
    foreach ($services as $class => $info) {

      $key = $info['id'];

      $form[$key] = [
        '#type' => 'details',
        '#title' => $key,
        '#group' => 'services_vt',
      ];

      // Prepare general information.
      $general_items = [
        'endpoint' => $this->serviceSectionItem($info['endpoint'], 'API Endpoint'),
        'response' => $this->serviceSectionItem($info['format'], 'Provider Response'),
        'discovery' => $this->serviceSectionItem($info['discovery'], 'Discovery supports'),
      ];

      $form[$key]['general'] = [
        '#theme' => 'item_list',
        '#title' => $key . ' oEmbed service',
        '#items' => $general_items,
      ];

      // URL schemes information.
      $url_schemes_title = $this->formatPlural(count($info['patterns']), 'URL scheme', 'URL schemes');
      $form[$key]['scheme'] = [
        '#theme' => 'item_list',
        '#title' => $url_schemes_title,
        '#items' => $info['patterns'],
      ];
      // Examples.
      foreach ($info['examples'] as $index => $value) {
        $form[$key]['examples'][$index] = [
          '#type' => 'fieldset',
          '#title' => 'Response: ' . $value['url'],
        ];

        $form[$key]['examples'][$index][] = [
          '#theme' => 'oembed_filter_example',
          '#html' => Markup::create($value['html']),
          '#raw' => $value['html'],
        ];
      }
    }

    return $form;
  }

  /**
   * Format text for section.
   *
   * @return string
   *   Formatting section.
   */
  private function serviceSectionItem($value, $prefix) {
    return $this->t('!prefix: <span>!value</span>', [
      '!prefix' => $prefix,
      '!value' => $value,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
