<?php

/**
 * @file
 * Contains \Drupal\oembed_filter\Tests\DefaultController.
 */

namespace Drupal\oembed_filter\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the oembed_filter module.
 */
class DefaultControllerTest extends WebTestBase {
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "oembed_filter DefaultController's controller functionality",
      'description' => 'Test Unit for module oembed_filter and controller DefaultController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests oembed_filter functionality.
   */
  public function testDefaultController() {
    // Check that the basic functions of module oembed_filter.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
